from celery import Celery

app = Celery('Calculation', broker='amqp://guest:guest@localhost:5672')

app.conf.task_default_exchange = 'Calculation'
app.conf.task_default_routing_key = 'Calculation'
app.conf.task_default_queue = 'Calculation'


@app.task()
def Calculation(a, operator, b):

    choice = operator
    #choice = map(int, input().split())
    #type(choice)

    if choice == '+':
        return a + b

    elif choice == '-':
        return a - b

    elif choice == '*':
        return a * b

    elif choice == '/':
        return a / b

    else:
        print("Invalid Input")